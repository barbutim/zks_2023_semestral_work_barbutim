package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.vet.Vets;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ZksTests {

	private static final String TEST_OWNER_STRING = "id = 1, " +
		"new = false, " +
		"lastName = 'Franklin', " +
		"firstName = 'George', " +
		"address = '110 W. Liberty St.', " +
		"city = 'Madison', " +
		"telephone = '6085551023'";

	private static final int TEST_OWNER_ID = 1;

	private static final int TEST_PET_ID = 1;

	private static final int TEST_VET_ID = 3;

	private static final int TEST_VET_NR_SPECIALTIES = 2;

	protected PetTypeFormatter petTypeFormatter;

	@Autowired
	protected OwnerRepository ownerRepository;

	@Autowired
	protected VetRepository vetRepository;

	@BeforeEach
	void setup() {
		petTypeFormatter = new PetTypeFormatter(ownerRepository);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner/owner_constructor/owner_2-way.csv", numLinesToSkip = 1)
	@Tag("OwnerTest")
	public void ownerConstructor2WayTest(String firstName, String lastName, String address, String city, String telephone, boolean valid) {
		Owner owner;
		try {
			owner = new Owner(firstName, lastName, address, city, telephone);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner).isNotNull();
		assertThat(owner.getFirstName()).isEqualTo(firstName);
		assertThat(owner.getLastName()).isEqualTo(lastName);
		assertThat(owner.getAddress()).isEqualTo(address);
		assertThat(owner.getCity()).isEqualTo(city);
		assertThat(owner.getTelephone()).isEqualTo(telephone);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner/owner_constructor/owner_3-way.csv", numLinesToSkip = 1)
	@Tag("OwnerTest")
	public void ownerConstructor3WayTest(String firstName, String lastName, String address, String city, String telephone, boolean valid) {
		Owner owner;
		try {
			owner = new Owner(firstName, lastName, address, city, telephone);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner).isNotNull();
		assertThat(owner.getFirstName()).isEqualTo(firstName);
		assertThat(owner.getLastName()).isEqualTo(lastName);
		assertThat(owner.getAddress()).isEqualTo(address);
		assertThat(owner.getCity()).isEqualTo(city);
		assertThat(owner.getTelephone()).isEqualTo(telephone);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/pet/pet_constructor.csv", numLinesToSkip = 1)
	@Tag("PetTest")
	public void petConstructorTest(String id, String birthDate, String name, String type, boolean valid) {
		Pet pet;
		try {
			pet = new Pet(id, birthDate, name, type);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(pet).isNotNull();
		assertThat(pet.getId().toString()).isEqualTo(id);
		assertThat(pet.getBirthDate().toString()).isEqualTo(birthDate);
		assertThat(pet.getName()).isEqualTo(name);
		assertThat(pet.getType().toString()).isEqualTo(type);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/vet/vet_constructor.csv", numLinesToSkip = 1)
	@Tag("VetTest")
	public void vetConstructorTest(String firstName, String lastName, boolean valid) {
		Vet vet;
		try {
			vet = new Vet(firstName, lastName);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(vet).isNotNull();
		assertThat(vet.getFirstName()).isEqualTo(firstName);
		assertThat(vet.getLastName()).isEqualTo(lastName);
		assertThat(vet.getSpecialties()).isNotNull();
		assertThat(vet.getSpecialties()).isEmpty();
	}

	@Test
	@Tag("OwnerTest")
	public void ownerToStringTest() {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		String ownerString = owner.toString();
		int ownerStringLen = ownerString.length();
		assertThat(ownerString.substring(ownerStringLen - 141, ownerStringLen - 1))
			.isEqualTo(TEST_OWNER_STRING);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner/owner_address.csv", numLinesToSkip = 1)
	@Tag("OwnerTest")
	public void setOwnerAddressTest(String address, boolean valid) {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		assertThat(owner.getAddress()).isNotNull();
		String ownerAddress = owner.getAddress();
		try {
			owner.setAddress(address);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(owner.getAddress()).isEqualTo(ownerAddress);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner.getAddress()).isEqualTo(address);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner/owner_city.csv", numLinesToSkip = 1)
	@Tag("OwnerTest")
	public void setOwnerCityTest(String city, boolean valid) {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		assertThat(owner.getCity()).isNotNull();
		String ownerCity = owner.getCity();
		try {
			owner.setCity(city);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(owner.getCity()).isEqualTo(ownerCity);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner.getCity()).isEqualTo(city);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner/owner_telephone.csv", numLinesToSkip = 1)
	@Tag("OwnerTest")
	public void setOwnerTelephoneTest(String telephone, boolean valid) {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		assertThat(owner.getTelephone()).isNotNull();
		String ownerTelephone = owner.getTelephone();
		try {
			owner.setTelephone(telephone);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(owner.getTelephone()).isEqualTo(ownerTelephone);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner.getTelephone()).isEqualTo(telephone);
	}

	@Test
	@Tag("PetTest")
	public void getPetByIdTest() {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		Pet pet = owner.getPet(TEST_PET_ID);
		assertThat(pet).isNotNull();
		assertThat(pet.getName()).isEqualTo("Leo");
		assertThat(pet.getId()).isEqualTo(TEST_PET_ID);
		assertThat(pet.getBirthDate()).isEqualTo("2010-09-07");
		assertThat(pet.getType().toString()).isEqualTo("cat");
		assertThat(pet.getVisits()).isEmpty();
		assertThat(owner.getPet(TEST_PET_ID + 1)).isNull();
	}

	@Test
	@Tag("PetTest")
	public void getPetByNameTest() {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		Pet pet = owner.getPet(TEST_PET_ID);
		assertThat(pet).isNotNull();
		String petName = pet.getName();
		Pet petByName = owner.getPet(petName);
		assertThat(petByName.getName()).isEqualTo(petName);
		assertThat(owner.getPet("Basil")).isNull();
	}

	@Test
	@Tag("PetTypeFormatterTest")
	public void petTypeFormatterPrintTest() {
		PetType petType = new PetType();
		petType.setName("caique");
		String petTypeName = petTypeFormatter.print(petType, Locale.ENGLISH);
		assertThat(petTypeName).isEqualTo("caique");
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/pet/pet_type.csv", numLinesToSkip = 1)
	@Tag("PetTypeFormatterTest")
	public void petTypeFormatterParseTest(String petTypeName, boolean valid) {
		PetType petType = new PetType();
		petType.setName(petTypeName);
		PetType petTypeParsed;
		try {
			petTypeParsed = petTypeFormatter.parse(petTypeName, Locale.ENGLISH);
		} catch (ParseException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(petTypeParsed.getName()).isEqualTo(petTypeName);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/pet/pet_name.csv", numLinesToSkip = 1)
	@Tag("PetTest")
	public void setPetNameTest(String name, boolean valid) {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		Pet pet = owner.getPet(TEST_PET_ID);
		assertThat(pet).isNotNull();
		assertThat(pet.getName()).isNotNull();
		String petName = pet.getName();
		try {
			pet.setName(name);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(pet.getName()).isEqualTo(petName);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(pet.getName()).isEqualTo(name);
	}

	@Test
	@Tag("OwnerTest")
	public void addPetToOwnerTest() {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		assertThat(owner.getPets().size()).isEqualTo(TEST_PET_ID);
		Pet pet = new Pet();
		owner.addPet(pet);
		assertThat(owner.getPets().size()).isEqualTo(TEST_PET_ID + 1);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/visit/visit_date.csv", numLinesToSkip = 1)
	@Tag("VisitTest")
	public void setVisitDateTest(String date, boolean valid) {
		Visit visit = new Visit();
		assertThat(visit).isNotNull();
		assertThat(visit.getDate()).isNotNull();
		LocalDate visitDate = visit.getDate();
		try {
			visit.setDateCustom(date);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(visit.getDate()).isEqualTo(visitDate);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(visit.getDate().toString()).isEqualTo(date);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/visit/visit_description.csv", numLinesToSkip = 1)
	@Tag("VisitTest")
	public void setVisitDescriptionTest(String description, boolean valid) {
		Visit visit = new Visit();
		assertThat(visit).isNotNull();
		assertThat(visit.getDescription()).isNull();
		try {
			visit.setDescription(description);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(visit.getDescription()).isNull();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(visit.getDescription()).isEqualTo(description);
	}

	@Test
	@Tag("PetTest")
	public void addVisitToPetTest() {
		Pet pet = new Pet();
		assertThat(pet).isNotNull();
		assertThat(pet.getVisits()).isEmpty();
		Visit visit = new Visit();
		assertThat(visit).isNotNull();
		pet.addVisit(visit);
		assertThat(pet.getVisits().size()).isEqualTo(1);
		assertThat(pet.getVisits().contains(visit)).isTrue();
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner/owner_add_visit.csv", numLinesToSkip = 1)
	@Tag("OwnerTest")
	public void addVisitToOwnerTest(String petId, String visit, boolean valid) {
		Owner owner = ownerRepository.findById(TEST_OWNER_ID);
		assertThat(owner).isNotNull();
		try {
			owner.addVisitCustom(petId, visit);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner.getPet(Integer.parseInt(petId)).getVisits()).isNotEmpty();
		assertThat(owner.getPet(Integer.parseInt(petId)).getVisits().stream().anyMatch(
			v -> v.getDescription().equals(visit))).isTrue();
	}

	@Test
	@Tag("VetTest")
	public void getNrOfSpecialtiesTest() {
		Vet vet = vetRepository.findById(TEST_VET_ID);
		assertThat(vet).isNotNull();
		assertThat(vet.getNrOfSpecialties()).isEqualTo(TEST_VET_NR_SPECIALTIES);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/vet/vet_add_specialty.csv", numLinesToSkip = 1)
	@Tag("VetTest")
	public void addSpecialtyToVetTest(String specialtyName, boolean valid) {
		Vet vet = vetRepository.findById(TEST_VET_ID);
		assertThat(vet).isNotNull();
		int nrOfSpecialties = vet.getNrOfSpecialties();
		assertThat(nrOfSpecialties).isEqualTo(TEST_VET_NR_SPECIALTIES);
		assertThat(vet.getSpecialties().stream().noneMatch(
			specialty -> specialty.getName().equals(specialtyName))).isTrue();
		try {
			vet.addSpecialtyCustom(specialtyName);
		} catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(vet.getNrOfSpecialties()).isEqualTo(TEST_VET_NR_SPECIALTIES + 1);
		assertThat(vet.getSpecialties().stream().anyMatch(
			specialty -> specialty.getName().equals(specialtyName))).isTrue();
	}

	@Test
	@Tag("VetTest")
	public void getSpecialtiesTest() {
		Vet vet = vetRepository.findById(TEST_VET_ID);
		assertThat(vet).isNotNull();
		List<Specialty> vetSpecialties = vet.getSpecialties();
		assertThat(vetSpecialties).isNotNull();
		assertThat(vetSpecialties.size()).isEqualTo(TEST_VET_NR_SPECIALTIES);
		assertThat(vetSpecialties.get(0).getName()).isEqualTo("dentistry");
		assertThat(vetSpecialties.get(1).getName()).isEqualTo("surgery");
	}

	@Test
	@Tag("VetTest")
	public void vetsTest() {
		Vets vets = new Vets();
		assertThat(vets.getVetList()).isEmpty();
	}
}
